<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\StandardLibrary;

/**
 * Extension of \DateTime standard phplibrary
 * @package Harbinger
 * @subpackage StandarLibrary
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class DateTime extends \DateTime
{

    /**
     * Create a DateTime object based on string
     *
     * based on parseDate function create by Hugo Ferreira da Silva
     * http://forum.imasters.com.br/topic/472065-conversao-de-datas-com-expressoes-regulares/?p=1874281
     *
     * @param string $string
     * @param boolean $currentDateTime
     * @return \Harbinger\StandardLibrary\DateTime
     * @throws \Harbinger\StandardLibrary\UnexpectedValueException If cannot parse the string
     */
    public static function createFromString($string , $currentDateTime = true)
    {
        $formats = array(
            'd/m/Y',
            'd/m/Y H',
            'd/m/Y H:i',
            'd/m/Y H:i:s',
            'd/m/Y H:i:s.u',
            'Y-m-d',
            'Y-m-d H',
            'Y-m-d H:i',
            'Y-m-d H:i:s',
            'Y-m-d H:i:s.u',
            'H d/m/Y',
            'H:i d/m/Y',
            'H:i:s d/m/Y',
            'H:i:s.u d/m/Y',
            'H Y-m-d',
            'H:i Y-m-d',
            'H:i:s Y-m-d',
            'H:i:s.u Y-m-d',
            'H',
            'H:i',
            'H:i:s',
            'H:i:s.u'
        );

        if (!$currentDateTime) {
            $callback = function (&$row) {
                $row = '!'.$row;
            };

            array_walk($formats, $callback);
        }

        $callBack = function ($format) use ($string) {
            return static::createFromFormat($format , $string) !== false;
        };

        foreach (array_filter($formats , $callBack) as $format) {
            return static::createFromDateTimeInterface(static::createFromFormat($format , $string));
        }

        throw new UnexpectedValueException(sprintf('Invalid date \'%s\'' , $string));
    }

    /**
     * Create a \Harbinger\StandardLibrary\DateTime object based on a DateTimeInterface object
     * @param DateTimeInterface $dateTime
     * @return \Harbinger\StandardLibrary\DateTime
     */
    public static function createFromDateTimeInterface(\DateTimeInterface $dateTime)
    {
        return new static($dateTime->format('Y-m-d H:i:s.u'));
    }

    /**
     * Returns the difference between two DateTimeInterface objects
     * @param \DateTimeInterface $object
     * @param boolean $absolute
     * @return \Harbinger\StandardLibrary\DateInterval
     */
    public function diff($object , $absolute = false)
    {
        return DateInterval::createFromDateInterval(parent::diff($object , $absolute));

    }
}
