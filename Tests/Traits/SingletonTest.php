<?php

namespace Harbinger\StandardLibrary\Traits\Tests;

use \Harbinger\StandardLibrary\Traits\Singleton;

class SingletonTest extends \PHPUnit_Framework_TestCase
{

    private $reflection;

    public function assertPreConditions()
    {
        $this->assertFalse(class_exists($class = Singleton::class) , 'Should be a trait: '.$class);
        $this->assertTrue(trait_exists($trait = Singleton::class) , 'Should be a trait: '.$trait);
        $this->assertTrue(trait_exists($trait = Singleton::class) , 'Should be a trait: '.$trait);
        $this->assertTrue(trait_exists($trait = Singleton::class) , 'Should be a trait: '.$trait);
    }

    public function setUp()
    {
        $this->reflection = new \ReflectionClass(Singleton::class);
    }

    public function testSingletonMethodsWhatShouldBePrivateAndFinal()
    {
        $methods[] = $this->reflection->getConstructor();
        $methods[] = $this->reflection->getMethod('__wakeup');
        $methods[] = $this->reflection->getMethod('__clone');

        foreach ($methods as $method) {
            $methodName = $method->getName();

            $this->assertTrue($method->isPrivate() , sprintf('%s should be private' , $methodName));
            $this->assertTrue($method->isFinal() , sprintf('%s should be final' , $methodName));
        }
    }


    public function testSingletonMethodsWhatShouldBeStaticPublicAndFinal()
    {
        $methods[] = $this->reflection->getMethod('getInstance');

        foreach ($methods as $method) {
            $methodName = $method->getName();

            $this->assertTrue($method->isPublic() , sprintf('%s should be public' , $methodName));
            $this->assertTrue($method->isFinal() , sprintf('%s should be final' , $methodName));
            $this->assertTrue($method->isStatic() , sprintf('%s should be static' , $methodName));
        }
    }


    public function testSingletonMethodsWhatShouldBeProtectedAndFinal()
    {
        $methods[] = $this->reflection->getMethod('init');

        foreach ($methods as $method) {
            $methodName = $method->getName();

            $this->assertTrue($method->isProtected() , sprintf('%s should be protected' , $methodName));
            $this->assertFalse($method->isFinal() , sprintf('%s should be inheritable' , $methodName));
        }
    }
}
