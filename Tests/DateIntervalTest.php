<?php

namespace Harbinger\StandardLibrary\Tests;

use \Harbinger\StandardLibrary\DateInterval;

class DateIntervalTest extends \PHPUnit_Framework_TestCase
{


    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = DateInterval::class) , 'Class not found: '.$class);

        $this->assertInstanceOf(
            $instance = \DateInterval::class,
            $class = new DateInterval('PT0S'),
            sprintf('%s should be instance of %s' , get_class($class) , $instance)
        );

        $this->assertNotInstanceOf(
            $instance = DateInterval::class,
            $class = new \DateInterval('PT0S'),
            sprintf('%s should\'nt be instance of %s' , get_class($class) , $instance)
        );
    }

    public function testSumDataIntervalWithValidParametersShouldWork()
    {
        $this->assertInstanceOf(
            \DateInterval::class,
            $result = DateInterval::sum(new \DateInterval('PT0S') , new \DateInterval('PT0S')),
            sprintf('%s should be instance of \DateInterval' , print_r($result , true))
        );

        $this->assertEquals(
            $result = new \DateInterval('PT0S'),
            new \DateInterval('PT0S'),
            sprintf('%s should be equal zero' , print_r($result , true))
        );

        $this->assertEquals(
            $result = new DateInterval('PT0S'),
            $createFromDateInterval = DateInterval::createFromDateInterval($result),
            sprintf('%s should be equal %s' , print_r($createFromDateInterval , true) , print_r($result , true))
        );

        $dateInterval = new \DateInterval('PT0S');

        $args = array();
        for ($i = 0 ; $i < 100 ; $i++) {
            $args[] = $dateInterval;
        }

        $this->assertInstanceOf(
            \DateInterval::class,
            $result = DateInterval::sum(...$args),
            sprintf('%s should be instance of \DateInterval' , print_r($result , true))
        );

        $this->assertEquals(
            new \DateInterval('PT0S'),
            $result,
            sprintf('%s should be equal zero' , print_r($result , true))
        );
    }
}
