<?php

namespace Harbinger\StandardLibrary\Tests;

use \Harbinger\StandardLibrary\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase
{

    private $registry;

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Registry::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {
        $this->registry = Registry::getInstance();
    }

    public function testMethodsWhatRegistryShouldHave()
    {
        $reflection = new \ReflectionClass(Registry::class);
        $methods[] = $reflection->getMethod('get');
        $methods[] = $reflection->getMethod('has');
        $methods[] = $reflection->getMethod('set');

        foreach ($methods as $method) {
            $methodName = $method->getName();

            $this->assertTrue($method->isPublic() , sprintf('%s should be public' , $methodName));
            $this->assertFalse($method->isFinal() , sprintf('%s should be inheritable' , $methodName));
        }
    }

    public function testMethodsHasGetAndSetShouldWorks()
    {
        $this->assertFalse($this->registry->has($key1 = 'key 1') , sprintf('The key %s should not exists' , $key1));
        $this->assertFalse($this->registry->has($key2 = 'key 2') , sprintf('The key %s should not exists' , $key2));
        $this->assertFalse($this->registry->has($key3 = 'key 3') , sprintf('The key %s should not exists' , $key3));

        $this->registry->set($key1 , $class1 = new \stdClass());
        $this->registry->set($key2 , $class2 = new \stdClass());
        $this->registry->set($key3 , $class3 = new \stdClass());

        $this->assertTrue($this->registry->has($key1) , sprintf('The key %s should exists' , $key1));
        $this->assertTrue($this->registry->has($key2) , sprintf('The key %s should exists' , $key2));
        $this->assertTrue($this->registry->has($key3) , sprintf('The key %s should exists' , $key3));

        $this->assertSame(
            $class1,
            $get1 = $this->registry->get($key1),
            sprintf('%s should be same as %s' , print_r($class1 , true) , print_r($get1 , true))
        );

        $this->assertSame(
            $class2,
            $get2 = $this->registry->get($key2),
            sprintf('%s should be same as %s' , print_r($class2 , true) , print_r($get2 , true))
        );

        $this->assertSame(
            $class3,
            $get3 = $this->registry->get($key3),
            sprintf('%s should be same as %s' , print_r($class3 , true) , print_r($get3 , true))
        );
    }
}
