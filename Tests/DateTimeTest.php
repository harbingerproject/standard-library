<?php

namespace Harbinger\StandardLibrary\Tests;

use \Harbinger\StandardLibrary\DateTime;

class DateTimeTest extends \PHPUnit_Framework_TestCase  {


    public function testCreateADateFromStringWithValidParametersShouldWork()
    {
        $this->assertInstanceOf(
            \DateTime::class,
            $result = DateTime::createFromString('2015-06-22'),
            sprintf('%s should be instance of \DateTime' , print_r($result , true))
        );

        $this->assertEquals(
            $result = new DateTime(),
            $createFromDateTimeInterface = DateTime::createFromDateTimeInterface($result),
            sprintf('%s should be equal %s' , print_r($createFromDateTimeInterface , true) , print_r($result , true))
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('Y-m-d' , '2015-06-22'),
            $result = DateTime::createFromString('2015-06-22'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertNotEquals(
            $dateTime = \DateTime::createFromFormat('!Y-m-d' , '2015-06-22'),
            $result = DateTime::createFromString('2015-06-22'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('!Y-m-d' , '2015-06-22'),
            $result = DateTime::createFromString('2015-06-22' , false),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('Y-m-d' , '2015-06-22'),
            $result = DateTime::createFromString('2015-06-22'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertNotEquals(
            $dateTime = new \DateTime('2015-06-22'),
            $result = DateTime::createFromString('2015-06-22'),
            sprintf(
                '%s should be different of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('2015-06-22'),
            $result = DateTime::createFromString('2015-06-22' , false),
            sprintf(
                '%s should be equal of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('2015-06-22 00:00:00'),
            $result = DateTime::createFromString('2015-06-22' , false),
            sprintf(
                '%s should be equal of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );
    }

    public function testCreateADateAndTimeFromStringWithValidParametersShouldWork()
    {
        $this->assertInstanceOf(
            \DateTime::class,
            $result = DateTime::createFromString('2015-06-22 09:45'),
            sprintf('%s should be instance of \DateTime' , print_r($result , true))
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('Y-m-d H:i' , '2015-06-22 09:45'),
            $result = DateTime::createFromString('2015-06-22 09:45'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('Y-m-d H:i' , '2015-06-22 09:45'),
            $result = DateTime::createFromString('2015-06-22 09:45'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('2015-06-22 09:45'),
            $result = DateTime::createFromString('2015-06-22 09:45'),
            sprintf(
                '%s should be equals of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('2015-06-22 09:45'),
            $result = DateTime::createFromString('2015-06-22 09:45' , false),
            sprintf(
                '%s should be equal of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('2015-06-22 09:45:00'),
            $result = DateTime::createFromString('2015-06-22 09:45' , false),
            sprintf(
                '%s should be equal of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );
    }

    public function testCreateATimeFromStringWithValidParametersShouldWork()
    {
        $this->assertInstanceOf(
            \DateTime::class,
            $result = DateTime::createFromString('09:45'),
            sprintf('%s should be instance of \DateTime' , print_r($result , true))
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('H:i' , '09:45'),
            $result = DateTime::createFromString('09:45'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = \DateTime::createFromFormat('H:i' , '09:45'),
            $result = DateTime::createFromString('09:45'),
            sprintf(
                '%s should be equal of %s \DateTime::createFromFormat ',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('09:45'),
            $result = DateTime::createFromString('09:45'),
            sprintf(
                '%s should be equals of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertNotEquals(
            $dateTime = new \DateTime('09:45:03'),
            $result = DateTime::createFromString('09:45:03' , false),
            sprintf(
                '%s should be differents of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('1970-01-01 09:45:03'),
            $result = DateTime::createFromString('09:45:03' , false),
            sprintf(
                '%s should be equals of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('09:45:03.000'),
            $result = DateTime::createFromString('09:45:03.000'),
            sprintf(
                '%s should be equals of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertNotEquals(
            $dateTime = new \DateTime('09:45:03.000'),
            $result = DateTime::createFromString('09:45:03.000' , false),
            sprintf(
                '%s should be differents of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );

        $this->assertEquals(
            $dateTime = new \DateTime('1970-01-01 09:45:03.123'),
            $result = DateTime::createFromString('09:45:03.123' , false),
            sprintf(
                '%s should be equal of %s new \DateTime',
                print_r($result , true),
                print_r($dateTime , true)
            )
        );
    }

    /**
     * @expectedException \Harbinger\StandardLibrary\UnexpectedValueException
     **/
    public function testCreateFromStringWithInvalidParametersShouldThrownAnException()
    {
        DateTime::createFromString('something');
    }
}
