<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\StandardLibrary\Test\Type;

use Harbinger\StandardLibrary\Type;
use Harbinger\StandardLibrary\Type\Enum;

class MockCalendar extends Enum
{
    const __DEFAULT = self::SUNDAY;

    /**
     * @var int
     */
    const SUNDAY = 0;

    /**
     * @var int
     */
    const MONDAY = 1;

    /**
     * @var int
     */
    const TUESDAY = 2;

    /**
     * @var int
     */
    const WEDNESDAY = 3;

    /**
     * @var int
     */
    const THURSDAY = 4;

    /**
     * @var int
     */
    const FRIDAY = 5;

    /**
     * @var int
     */
    const SATURDAY = 6;
}

class EnumTest  extends \PHPUnit_Framework_TestCase
{

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Enum::class) , 'Class not found: '.$class);

        $mockCalendar = new MockCalendar();
        $this->assertTrue($mockCalendar instanceof Enum , 'Class not found: '.$class);
        $this->assertTrue($mockCalendar instanceof Type , 'Class not found: '.$class);
    }

    public function setUp()
    {

    }

    public function testMethodsWhatRegistryShouldHave()
    {
        $reflection = new \ReflectionClass(Enum::class);
        $methods[] = $reflection->getMethod('getConstList');
        $methods[] = $reflection->getMethod('equals');

        foreach ($methods as $method) {
            $methodName = $method->getName();

            $this->assertTrue($method->isPublic() , sprintf('%s should be public' , $methodName));
            $this->assertFalse($method->isFinal() , sprintf('%s should be inheritable' , $methodName));
        }
    }

    public function testMethodEqualsShouldWork()
    {
        $mockCalendar = new MockCalendar();

        $this->assertTrue(
            (string)$mockCalendar == MockCalendar::SUNDAY,
            sprintf('%s should be == to %s' , print_r($mockCalendar , true) , MockCalendar::SUNDAY)
        );
        $this->assertTrue(
            $mockCalendar->equals(MockCalendar::SUNDAY),
            sprintf('%s should be MockCalendar::equals to %s' , print_r($mockCalendar , true) , MockCalendar::SUNDAY)
        );
    }
}
