<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\StandardLibrary;

/**
 * Implementation of a data registry.
 * @package Harbinger
 * @subpackage StandardLibrary
 * @author Gabriel Heming <gabriel.heming@hotmail.com.br>
 **/
class Registry
{
    use \Harbinger\StandardLibrary\Traits\Singleton;

    /**
     * @var mixed[]
     **/
    private $registry = array();

    /**
     * Retrieve a value by his key
     * @param string $key.
     * @return mixed.
     * @throws \InvalidArgumentException If the key isn't registred
     **/
    public function get($key)
    {
        if (!isset($this->registry[$key])) {
            throw new InvalidArgumentException(sprintf("There\'s no value for key %s." , $key));
        }

        return $this->registry[$key];
    }

    /**
     * Checks if a key is setted in registry
     * @param string $key
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->registry[$key]);
    }

    /**
     * Define a pair key=value in registry
     * @param string $key.
     * @param mixed $value
     **/
    public function set($key , $value)
    {
        $this->registry[$key] = $value;
    }
}
