# StandardLibrary

## Requirements

* PHP 5.6+

## Changelog

### 2.0.1 ###

* Add interface \Harbinger\StandardLibrary\Type;
* Add implementation of EnumType as \Harbinger\StandardLibrary\Type\Enum;
* Add new Exception \Harbinger\StandardLibrary\InvalidArgumentException;
* Add tests for EnumType.

### 2.0.0 ###

* PHP 7 support;

### 1.0.4 ###

* Add interface \Harbinger\StandardLibrary\Type;
* Add implementation of EnumType as \Harbinger\StandardLibrary\Type\Enum;
* Add new Exception \Harbinger\StandardLibrary\InvalidArgumentException;
* Add tests for EnumType.

### 1.0.3 ###

* Updated style coding to match with PSR-1 and PSR-2 (except spaces before comma: interfaces and functions args);
* Updated \Harbinger\SatandardLibrary\DateTime and \Harbinger\SatandardLibrary\DateInterval to always return an
instance of himself;
* Add new test;
* Changed self keyword for static (late static binding);
* Removed phpDocumentor version tag.

