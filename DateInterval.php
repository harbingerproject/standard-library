<?php

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\StandardLibrary;

/**
 * Extension of \DateTime standard phplibrary
 * @package Harbinger
 * @subpackage StandarLibrary
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 **/
class DateInterval extends \DateInterval
{

    /**
     * Sum dateIntervals
     * @param \DateInterval ...$addends
     * @return \DateInterval
     **/
    public static function sum(\DateInterval ...$addends)
    {
        $start = clone $end = new \DateTime();

        foreach ($addends as $addend) {
            $end->add($addend);
        }

        return $end->diff($start , true);
    }

    public static function createFromDateInterval(\DateInterval $dateInterval)
    {
        $newDateInterval = new static('PT0S');

        foreach ($dateInterval as $key => $value) {
            $newDateInterval->$key = $value;
        }

        return $newDateInterval;
    }

    public function recalculate()
    {
        $to = clone $from = new DateTime();
        $to = $to->add($this);
        $diff = $from->diff($to);

        foreach ($diff as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }
}
